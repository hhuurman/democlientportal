const { defineConfig } = require("cypress");
const createBundler = require("@bahmutov/cypress-esbuild-preprocessor");
const addCucumberPreprocessorPlugin =
    require("@badeball/cypress-cucumber-preprocessor").addCucumberPreprocessorPlugin;
const createEsbuildPlugin =
    require("@badeball/cypress-cucumber-preprocessor/esbuild").createEsbuildPlugin;


module.exports = defineConfig({
  e2e: {
    async setupNodeEvents(on, config) {
      const bundler = createBundler({
        plugins: [createEsbuildPlugin(config)],
      });

      on("file:preprocessor", bundler);
      await addCucumberPreprocessorPlugin(on, config);

      return config;
    },
    specPattern: "cypress/e2e/features/*.feature",
    specPattern: "cypress/e2e/features/*/*.feature",
    chromeWebSecurity: false,
    //baseUrl: 'https://accept.maqqie.app/',
    env:{
      acc:"https://accept.maqqie.app/Maqqie/#/auth_login",
      tst:"https://test.maqqie.app/Maqqie/#/auth_login",
      dev:"https://dev.maqqie.app/Maqqie/#/auth_signup",
      CPaccMP:"https://accept-clientportal.miseenplace.nl/",
      CPtstMP:"https://test-clientportal.miseenplace.nl/",
      CPdevMP:"https://dev-clientportal.miseenplace.nl/",
      CPacc:"https://accept-client-portal.maqqie.app/",
      CPtst:"https://test-client-portal.maqqie.app/",
      CPdev:"https://dev-client-portal.maqqie.app/"
    },
    users:{
      Admin:{
        username:"hhuurman@maqqie.nl",
        password:"Test1234",
        pin:"11111"
      },
      Employee1:{
        username:"1maqqietest+727EmpCyTesting93@gmail.com",
        password:"MaqqieTest123",
        pin:"11111"
      },
      Employee2:{
        username:"1maqqietest+923EmpCyTesting100@gmail.com",
        password:"MaqqieTest123",
        pin:"11111"
      },
      Employee3:{
        username:"1maqqietest+951EmpCyTesting20@gmail.com",
        password:"MaqqieTest123",
        pin:"11111"
      },
      Employee4:{
        username:"1maqqietest+4EmpCyTesting16@gmail.com",
        password:"MaqqieTest123",
        pin:"11111"
      },
      CPMPADMIN:{
        username:"maqqie.accept+MEP_Administrator@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPMPPlanning:{
        username:"maqqie.accept+MEP_Planning_Manager@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPMPFloorManager:{
        username:"maqqie.accept+MEP_Floor_manager@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPMPBookkeeper:{
        username:"maqqie.accept+MEP_Bookkeeper@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPADMIN:{
        username:"maqqie.accept+maqqie_Administrator@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPPlanning:{
        username:"maqqie.accept+maqqie_Planning_Manager@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPFloorManager:{
        username:"maqqie.accept+maqqie_Floor_manager@gmail.com",
        password:"test",
        pin:"11111"
      },
      CPBookkeeper:{
        username:"maqqie.accept+maqqie_Bookkeeper@gmail.com",
        password:"test",
        pin:"11111"
      }
    }
  }


});



