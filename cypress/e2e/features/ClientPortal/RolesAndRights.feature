Feature: Roles and rights
  Scenario: Check rights for administrator MEP
    Given I am logged in as a Administrator for MEP
    Then  I have the correct rights as Administrator for MEP

  Scenario: Check rights for Planning Manager MEP
    Given I am logged in as a Planning Manager for MEP
    Then  I have the correct rights as Planning Manager for MEP

  Scenario: Check rights for Floor Manager MEP
    Given I am logged in as a Floor Manager for MEP
    Then  I have the correct rights as Floor Manager for MEP

  Scenario: Check rights for Bookkeeper MEP
    Given I am logged in as a Bookkeeper for MEP
    Then  I have the correct rights as Bookkeeper for MEP

  Scenario: Check rights for administrator
    Given I am logged in as a Administrator
    Then  I have the correct rights as Administrator

  Scenario: Check rights for Planning Manager
    Given I am logged in as a Planning Manager
    Then  I have the correct rights as Planning Manager

  Scenario: Check rights for Floor Manager
    Given I am logged in as a Floor Manager
    Then  I have the correct rights as Floor Manager

  Scenario: Check rights for Bookkeeper
    Given I am logged in as a Bookkeeper
    Then  I have the correct rights as Bookkeeper


