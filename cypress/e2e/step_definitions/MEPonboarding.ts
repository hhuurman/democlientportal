import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";



Given("I open the MEP onboarding flow", () => {

        cy.visit('https://accept-mep.maqqie.app/mep/#/signup/chooseproject')

});



When("I select the project Festivals", () => {
        cy.get('input').click().type('{selectAll}{del}Festivals');
        cy.get('.list-item__title').click();
        cy.get('.btn').click();

});


When("I fill in my personal details", () => {
        cy.get(':nth-child(1) > :nth-child(2) > .input-wrapper > .invalid').type('TestMepEmployee');
        cy.get(':nth-child(3) > .input-wrapper > .invalid').type('hhuurman@maqqie.nl');
        cy.get(':nth-child(4) > .mq-form-row-right > .input-wrapper > .invalid').type('648389265');
        cy.get(':nth-child(5) > .input-wrapper > .invalid').type('21');
        cy.get('.mq-form-row-left > .input-wrapper > .invalid').type('3075RS');
        cy.get('.mq-form-row-right > .input-wrapper > .invalid').type('rotterdam');
        cy.get('.mq-checkbox > input').click();
        cy.get('input[type="checkbox"]').click();





});

