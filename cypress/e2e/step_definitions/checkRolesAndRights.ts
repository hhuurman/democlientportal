import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Then("I have the correct rights as Administrator for MEP", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
        //Check data from Shifts overview page
        cy.get('.shifts-overview__title').contains('Shifts overview');
        cy.get('.shifts-overview__header > .btn').contains(' + new request');
        cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
        cy.get('.user__name').contains('Arena');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('exist');
    cy.get('[data-icon="clock"]').click();
        //Check data from Registrations overview
        cy.get('.reg-over__title').contains('Registrations overview');
        cy.get('.user__name').contains('Arena');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
        //
        cy.get('.flexpool__title').contains('Flexpool');
        cy.get('.user__name').contains('Arena');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Planning Manager for MEP", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
    //Check data from Shifts overview page
    cy.get('.shifts-overview__title').contains('Shifts overview');
    cy.get('.shifts-overview__header > .btn').contains(' + new request');
    cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
    cy.get('.user__name').contains('Arena');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('not.exist');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('Arena');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Floor Manager for MEP", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
    //Check data from Shifts overview page
    cy.get('.shifts-overview__title').contains('Shifts overview');
    cy.get('.shifts-overview__header > .btn').contains(' + new request');
    cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
    cy.get('.user__name').contains('Arena');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('not.exist');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('Arena');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Bookkeeper for MEP", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('not.exist');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('exist');
    cy.get('[data-icon="clock"]').click();
    //Check data from Registrations overview
    cy.get('.reg-over__title').contains('Registrations overview');
    cy.get('.user__name').contains('Arena');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('Arena');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();

});

Then("I have the correct rights as Administrator", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
    //Check data from Shifts overview page
    cy.get('.shifts-overview__title').contains('Shifts overview');
    cy.get('.shifts-overview__header > .btn').contains(' + new request');
    cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
    cy.get('.user__name').contains('De Efteling');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('exist');
    cy.get('[data-icon="clock"]').click();
    //Check data from Registrations overview
    cy.get('.reg-over__title').contains('Registrations overview');
    cy.get('.user__name').contains('De Efteling');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('De Efteling');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Planning Manager", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
    //Check data from Shifts overview page
    cy.get('.shifts-overview__title').contains('Shifts overview');
    cy.get('.shifts-overview__header > .btn').contains(' + new request');
    cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
    cy.get('.user__name').contains('De Efteling');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('not.exist');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('De Efteling');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Floor Manager", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('exist');
    cy.get('[data-icon="calendar-days"]').click();
    //Check data from Shifts overview page
    cy.get('.shifts-overview__title').contains('Shifts overview');
    cy.get('.shifts-overview__header > .btn').contains(' + new request');
    cy.get('.breadcrumbs > ul > :nth-child(2)').contains('Shifts overview');
    cy.get('.user__name').contains('De Efteling');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('not.exist');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('De Efteling');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

Then("I have the correct rights as Bookkeeper", () => {
    //Check right for shift overview
    cy.get('[data-icon="calendar-days"]').should('not.exist');
    //check rights from Registrations overview
    cy.get('[data-icon="clock"]').should('exist');
    cy.get('[data-icon="clock"]').click();
    //Check data from Registrations overview
    cy.get('.reg-over__title').contains('Registrations overview');
    cy.get('.user__name').contains('De Efteling');
    //check rights from Flexpool
    cy.get('[data-icon="building"]').should('exist');
    cy.get('[data-icon="building"]').click();
    //
    cy.get('.flexpool__title').contains('Flexpool');
    cy.get('.user__name').contains('De Efteling');

    //check rights from Home
    cy.get('[data-icon="home"]').should('exist');
    cy.get('[data-icon="home"]').click();
});

