import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";
let JobName

before(() => {

});


Given("I get the jobs from Maqqie", () => {

    cy.request({
        method: 'GET',
        url: 'https://accept.maqqie.app/employee-service/shifts?page=0&pageSize=24&sort=startTimestamp%3Basc&excludeEmptyGigSlug=true&excludePastShifts=true', // baseUrl is prepend to URL
        headers: {
            'User-Agent': 'request'
        }
    })
        .its('body').then((body)=> {
            JobName = body._embedded[0].name
            cy.log(JobName)
        }
    );
    cy.visit('https://accept.maqqie.app/jobs/');
    cy.contains(JobName).click();
    cy.log(JobName)
});


after(() => {

});