//Login client portal
import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given("I am logged in as a Administrator for MEP", () => {
    cy.logincp(Cypress.config('users').CPMPADMIN, Cypress.env('CPaccMP'))
});

Given("I am logged in as a Planning Manager for MEP", () => {
    cy.logincp(Cypress.config('users').CPMPPlanning, Cypress.env('CPaccMP'))
});

Given("I am logged in as a Floor Manager for MEP", () => {
    cy.logincp(Cypress.config('users').CPMPFloorManager, Cypress.env('CPaccMP'))
});


Given("I am logged in as a Bookkeeper for MEP", () => {
    cy.logincp(Cypress.config('users').CPMPBookkeeper, Cypress.env('CPaccMP'))
});

Given("I am logged in as a Administrator", () => {
    cy.logincp(Cypress.config('users').CPADMIN, Cypress.env('CPacc'))
});
Given("I am logged in as a Planning Manager", () => {
    cy.logincp(Cypress.config('users').CPPlanning, Cypress.env('CPacc'))
});

Given("I am logged in as a Floor Manager", () => {
    cy.logincp(Cypress.config('users').CPFloorManager, Cypress.env('CPacc'))
});


Given("I am logged in as a Bookkeeper", () => {
    cy.logincp(Cypress.config('users').CPBookkeeper, Cypress.env('CPacc'))
});

Given("I am logged in as a employer", () => {
    cy.login(Cypress.config('users').Admin, Cypress.env('acc'))

});


Given("I am logged in as a employee", () => {
    cy.login(Cypress.config('users').Employee1, Cypress.env('acc'))

});


Given("I am logged in as a employee2", () => {
    cy.loginCP(Cypress.config('users').Employee2, Cypress.env('acc'))

});