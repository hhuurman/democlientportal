// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (user, env) => {
    cy.visit(env);
    cy.get("input[type='email']").type(user.username);
    cy.get("button[name='login_button']").click();
    cy.get("input[name='password_field']").type(user.password)
    cy.get('button[name="login_button"]').click();
    cy.get(".mq-tmp-pin-entry-container").type(user.pin);
});


Cypress.Commands.add('logincp', (user, env) => {
    cy.visit(env);
    cy.get('#username').type(user.username);
    cy.get('#password').type(user.password);
    cy.get('#kc-login').click();
});
